﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tesseract;

namespace VisualNovelTranslator
{
    public static class Common
    {
        const string infoTesseractPath = @"tessdatapath.txt";
        static string tesseractPath;
        static DebugTimer timer = new DebugTimer();
        static TesseractEngine ocr;

        public static string ImageToText(Bitmap image)
        {
            if (tesseractPath == null)
            {
                if (System.IO.File.Exists(infoTesseractPath))
                    tesseractPath = System.IO.File.ReadAllText(infoTesseractPath);
                if (string.IsNullOrEmpty(tesseractPath))
                    tesseractPath = "tessdata";
            }
            if (ocr == null)
            {
                ocr = new TesseractEngine(tesseractPath, "eng");
            }
            timer.Restart();
            var result = ocr.Process(image);
            timer.LogR("OCR Process");
            var text = result.GetText().Replace("\r", "").Replace("\n", " ").Trim();
            timer.LogR("OCR Get text");
            result.Dispose();
            timer.LogR("OCR Dispose");
            return text;
        }

        public static Bitmap CropImageP(Bitmap image, Rectangle boundsP)
        {
            Rectangle r = new Rectangle(
                (int)(image.Width * boundsP.X / 100),
                (int)(image.Height * boundsP.Y / 100),
                (int)((image.Width * boundsP.Width / 100) -
                (image.Width * boundsP.X / 100)),
                (int)((image.Height * boundsP.Height / 100) -
                (image.Height * boundsP.Y / 100)));

            Bitmap trgImage = new Bitmap(r.Width, r.Height);
            using (Graphics g = Graphics.FromImage(trgImage))
            {
                g.DrawImage(image, new Rectangle(0, 0, trgImage.Width, trgImage.Height),
                    r,
                    GraphicsUnit.Pixel);
            }

            return trgImage;
        }

        public static Bitmap ThresholdImageNew(Bitmap image, byte threshold)
        {
            var gray_matrix = new float[][] {
                new float[] { 0.299f, 0.299f, 0.299f, 0, 0 },
                new float[] { 0.587f, 0.587f, 0.587f, 0, 0 },
                new float[] { 0.114f, 0.114f, 0.114f, 0, 0 },
                new float[] { 0,      0,      0,      1, 0 },
                new float[] { 0,      0,      0,      0, 1 }
            };

            // Make the result bitmap.
            Bitmap bm = new Bitmap(image.Width, image.Height);

            // Make the ImageAttributes object and set the threshold.
            ImageAttributes attributes = new ImageAttributes();
            float thr = (float)threshold / 255;
            attributes.SetThreshold(thr);
            attributes.SetColorMatrix(new System.Drawing.Imaging.ColorMatrix(gray_matrix));

            // Draw the image onto the new bitmap
            // while applying the new ColorMatrix.
            Point[] points =
            {
                new Point(0, 0),
                new Point(image.Width, 0),
                new Point(0, image.Height),
            };


            Rectangle rect =
                new Rectangle(0, 0, image.Width, image.Height);
            using (Graphics gr = Graphics.FromImage(bm))
            {
                gr.DrawImage(image, points, rect,
                    GraphicsUnit.Pixel, attributes);
            }

            // Return the result.
            return bm;
        }

        public static void ThresholdImageOld(Bitmap bmp, byte threshold, int method, bool inverted)
        {
            Color c1 = inverted ? Color.White : Color.Black;
            Color c2 = inverted ? Color.Black : Color.White;
            Color px, result;
            float value;
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bmpData.PixelFormat) / 8;

            unsafe
            {
                byte* ptr = (byte*)bmpData.Scan0;

                for (int x = 0; x < bmp.Width; x++)
                {
                    for (int y = 0; y < bmp.Height; y++)
                    {
                        px = Color.FromArgb(ptr[1], ptr[2], ptr[3], ptr[0]);
                        //px = bmp.GetPixel(x, y);
                        value = 0;
                        switch (method)
                        {
                            case 0: { value = (Math.Max(Math.Max(px.R, px.G), px.B) + Math.Min(Math.Min(px.R, px.G), px.B)) / 2; break; }
                            case 1: { value = (px.R + px.G + px.B) / 3; break; }
                            case 2: { value = px.R * 0.21f + px.G * 0.72f + px.B * 0.07f; break; }
                        }

                        result = value < threshold ? c1 : c2;
                        //bmp.SetPixel(x, y, result);
                        ptr[0] = result.R;
                        ptr[1] = result.G;
                        ptr[2] = result.B;
                        ptr[3] = result.A;
                        ptr += bytesPerPixel;
                    }
                }

            }

            bmp.UnlockBits(bmpData);
        }

        public static string TranslateWithGoogle(string input, string srcLan, string trgLan)
        {
            try
            {
                GoogleTranslateFreeApi.GoogleTranslator t = new GoogleTranslateFreeApi.GoogleTranslator();
                var task = t.TranslateLiteAsync(input, GoogleTranslateFreeApi.Language.English, GoogleTranslateFreeApi.Language.Spanish);
                task.Wait();
                var r = task.Result;
                return r.MergedTranslation;


                //input = Uri.EscapeDataString(input);
                //string url = String.Format("http://www.google.com/translate_t?hl=en&ie=utf-8&text={0}&langpair={1}|{2}", input, srcLan, trgLan);
                //WebClient webClient = new WebClient();
                //webClient.Encoding = System.Text.Encoding.UTF8;
                //webClient.Headers.Add("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12");
                //webClient.Headers.Add("Accept", "*/*");
                //webClient.Headers.Add("Accept-Language", "en-gb,en;q=0.5");
                //webClient.Headers.Add("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");

                //var result = webClient.DownloadString(url);
                //string initPart = "TRANSLATED_TEXT='";
                //string endPart = "';var";
                
                //result = result.Substring(result.IndexOf(initPart) + initPart.Length);
                //result = result.Substring(0, result.IndexOf(endPart));

                ////string initPart = "<span id=result_box";
                ////string endPart1 = ">";
                ////string endPart2 = "</span>";

                ////result = result.Substring(result.IndexOf(initPart) + initPart.Length);
                ////result = result.Substring(result.IndexOf(endPart1) + endPart1.Length);
                ////result = result.Substring(result.IndexOf(endPart1) + endPart1.Length);
                ////result = result.Substring(0, result.IndexOf(endPart2));

                //result = result.Replace("\\x26", "&");
                //result = WebUtility.HtmlDecode(result);

                //return result;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return "ERROR";
            }
        }

        public static string TranslateWithDeepl(string input, string srcLan, string trgLan, int channel = 0)
        {
            input = HttpUtility.JavaScriptStringEncode(input);

            try
            {
                string url = "https://api.deepl.com/jsonrpc";
                WebClient webClient = new WebClient();
                webClient.Encoding = System.Text.Encoding.UTF8;
                webClient.Headers.Add("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12");
                webClient.Headers.Add("Accept", "*/*");
                webClient.Headers.Add("Accept-Language", "en-gb,en;q=0.5");
                webClient.Headers.Add("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
                webClient.Headers[HttpRequestHeader.ContentType] = "application/json";

                string cookieFileName = "deeplCookie.txt";
                if (!System.IO.File.Exists(cookieFileName))
                {
                    using (System.IO.File.Create(cookieFileName)) { }
                }

                string cookie = System.IO.File.ReadAllText(cookieFileName);
                webClient.Headers.Add("Cookie", cookie);

                // Request splitted sentences
                string splitRequest = "{{\"jsonrpc\":\"2.0\",\"method\": \"LMT_split_into_sentences\",\"params\":{{\"texts\":[\"{0}\"],\"lang\":{{\"lang_user_selected\":\"{1}\",\"user_preferred_langs\":[\"{1}\",\"{2}\"]}}}}}}";
                splitRequest = string.Format(splitRequest, input, srcLan, trgLan);

                string splitted = webClient.UploadString(url, splitRequest);
                JObject resSplitted = JObject.Parse(splitted);
                var sp = (JArray)resSplitted["result"]["splitted_texts"][0];
                var spArray = sp.ToObject<string[]>();

                // Send splitted sentences to translate
                string content = "{{\"jsonrpc\":\"2.0\",\"method\":\"LMT_handle_jobs\",\"params\":{{\"jobs\":[{0}],\"lang\":{{\"user_preferred_langs\":[\"{2}\",\"{1}\"],\"source_lang_user_selected\":\"{1}\",\"target_lang\":\"{2}\"}}}}}}";
                string jobTemplate = "{{\"kind\":\"default\",\"raw_en_sentence\":\"{0}\",\"quality\":\"fast\"}}";

                string[] jobs = spArray.Select(x => string.Format(jobTemplate, HttpUtility.JavaScriptStringEncode(x))).ToArray();

                content = string.Format(content, string.Join(",", jobs), srcLan, trgLan);

                string result = webClient.UploadString(url, content);
                JObject resObj = JObject.Parse(result);

                var translations = (JArray)resObj["result"]["translations"];
                var trans = translations.Select(x => (string)x["beams"][channel]["postprocessed_sentence"]).ToArray();

                string str = string.Join(" ", trans);

                return str;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                if (e.Message.Contains("403"))
                {
                    return "Please UPDATE cookies (in file deeplCookie.txt)";
                }
                return "ERROR";
            }
        }
    }
}
