﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualNovelTranslator
{
    class DebugTimer
    {
        private System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
        private List<string> _stackMessages = new List<string>();

        public DebugTimer()
        {
        }

        public DebugTimer(bool start)
        {
#if DEBUG
            if (start)
            {
                timer.Start();
            }
#endif
        }

        public void Start()
        {
#if DEBUG
            timer.Start();
#endif
        }

        public void Restart()
        {
#if DEBUG
            timer.Restart();
#endif
        }

        private string GetMessage(string message)
        {
            return string.Concat(timer.ElapsedMilliseconds, " - ", message);
        }

        public void Log(string message)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine(GetMessage(message));
#endif
        }

        public void LogR(string message)
        {
#if DEBUG
            Log(message);
            timer.Restart();
#endif
        }

        public void EnqueueBuffer(string message)
        {
#if DEBUG
            _stackMessages.Add(GetMessage(message));
#endif
        }

        public void EnqueueBufferR(string message)
        {
#if DEBUG
            EnqueueBuffer(message);
            timer.Restart();
#endif
        }

        public void LogBuffer()
        {
#if DEBUG
            foreach (var msg in _stackMessages)
            {
                System.Diagnostics.Debug.WriteLine(msg);
            }
#endif
        }

    }
}
