﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualNovelTranslator
{
    public partial class SettingsArea : Form
    {
        private Bitmap srcImage;
        private Bitmap trgImage;

        public SettingsArea()
        {
            InitializeComponent();
            cb_thresholdMethod.SelectedIndex = 1;
        }

        private void RefreshAreaComboBox()
        {
            cb_areaIndex.Items.Clear();
            for(int i=0; i < Config.settings.areaSettings.Count; i++)
            {
                cb_areaIndex.Items.Add(i.ToString());
            }
        }

        private void SettingsArea_Load(object sender, EventArgs e)
        {
            LoadArea(Config.settings.currentArea);
            RefreshAreaComboBox();
            cb_areaIndex.SelectedIndex = Config.settings.currentArea;
        }

        private void UpdateImage()
        {
            System.Diagnostics.Debug.WriteLine("UPDATING IMAGE...");
            if (srcImage == null)
                return;

            Rectangle cropArea = new Rectangle((int)tb_cropX.Value, (int)tb_cropY.Value,
                (int)tb_cropW.Value, (int)tb_cropH.Value);

            trgImage = Common.CropImageP(srcImage, cropArea);

            if (cb_thresholdEnabled.Checked)
            {
                //Common.ThresholdImage(trgImage, (byte)tb_threshold.Value, cb_thresholdMethod.SelectedIndex, cb_thresholdInverted.Checked);
                trgImage = Common.ThresholdImageNew(trgImage, (byte)tb_threshold.Value);
            }

            pictureBox1.Image = trgImage;
        }

        private void bt_test_Click(object sender, EventArgs e)
        {
            bt_test.Enabled = false;

            ScreenCapture sc = new ScreenCapture();
            srcImage = sc.CaptureScreen();

            UpdateImage();

            var result = Common.ImageToText(trgImage);
            tb_result.Text = result;

            bt_test.Enabled = true;
        }

        private void componentChanged(object sender, EventArgs e)
        {
            UpdateImage();
        }

        private void bt_create_Click(object sender, EventArgs e)
        {
            int index = Config.settings.areaSettings.Count;
            Config.settings.areaSettings.Add(new Config.AreaSettings());
            SaveArea(index);
            RefreshAreaComboBox();
            cb_areaIndex.SelectedIndex = index;
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            SaveArea(cb_areaIndex.SelectedIndex);
        }

        private void bt_delete_Click(object sender, EventArgs e)
        {
            Config.settings.areaSettings.RemoveAt(cb_areaIndex.SelectedIndex);
            RefreshAreaComboBox();
            cb_areaIndex.SelectedIndex = -1;
        }

        private bool LoadArea(int index)
        {
            if (index < 0 || index >= Config.settings.areaSettings.Count)
                return false;

            var settings = Config.settings.areaSettings[index];
            pictureBox1.Image = null;
            tb_result.Text = "";

            tb_cropX.Value = settings.boundsP.X;
            tb_cropY.Value = settings.boundsP.Y;
            tb_cropW.Value = settings.boundsP.Width;
            tb_cropH.Value = settings.boundsP.Height;
            tb_threshold.Value = settings.thresholdValue;
            cb_thresholdEnabled.Checked = settings.thresholdEnabled;
            cb_thresholdInverted.Checked = settings.thresholdInverted;
            cb_thresholdMethod.SelectedIndex = settings.thresholdMethod;

            return true;
        }

        private bool SaveArea(int index)
        {
            if (index < 0 || index >= Config.settings.areaSettings.Count)
                return false;

            var settings = Config.settings.areaSettings[index];
            settings.boundsP = new Rectangle((int)tb_cropX.Value, (int)tb_cropY.Value,
                (int)tb_cropW.Value, (int)tb_cropH.Value);

            settings.thresholdEnabled = cb_thresholdEnabled.Checked;
            settings.thresholdValue = (int)tb_threshold.Value;
            settings.thresholdMethod = cb_thresholdMethod.SelectedIndex;
            settings.thresholdInverted = cb_thresholdInverted.Checked;
            return true;
        }

        private void cb_areaIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadArea(cb_areaIndex.SelectedIndex);
        }
    }
}
