﻿namespace VisualNovelTranslator
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.areasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lb_imageCounter = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lb_Translator2Index = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lb_Translator2Key = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lb_State = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lb_RunKey = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_refreshTime = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.lb_TranslatorIndex = new System.Windows.Forms.Label();
            this.lb_TranslatorKey = new System.Windows.Forms.Label();
            this.lb_AreaKey = new System.Windows.Forms.Label();
            this.lb_AreaIndex = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_originalText = new System.Windows.Forms.TextBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tb_translatedText = new System.Windows.Forms.TextBox();
            this.tb_translatedText2 = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.bt_target_font = new System.Windows.Forms.ToolStripButton();
            this.bt_target_color = new System.Windows.Forms.ToolStripButton();
            this.bt_target_bgColor = new System.Windows.Forms.ToolStripButton();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb_refreshTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(787, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openConfigToolStripMenuItem,
            this.saveConfigToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openConfigToolStripMenuItem
            // 
            this.openConfigToolStripMenuItem.Name = "openConfigToolStripMenuItem";
            this.openConfigToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.openConfigToolStripMenuItem.Text = "Load config";
            this.openConfigToolStripMenuItem.Click += new System.EventHandler(this.openConfigToolStripMenuItem_Click);
            // 
            // saveConfigToolStripMenuItem
            // 
            this.saveConfigToolStripMenuItem.Name = "saveConfigToolStripMenuItem";
            this.saveConfigToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.saveConfigToolStripMenuItem.Text = "Save config";
            this.saveConfigToolStripMenuItem.Click += new System.EventHandler(this.saveConfigToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.areasToolStripMenuItem,
            this.keysToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // areasToolStripMenuItem
            // 
            this.areasToolStripMenuItem.Name = "areasToolStripMenuItem";
            this.areasToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.areasToolStripMenuItem.Text = "Areas";
            this.areasToolStripMenuItem.Click += new System.EventHandler(this.areasToolStripMenuItem_Click);
            // 
            // keysToolStripMenuItem
            // 
            this.keysToolStripMenuItem.Name = "keysToolStripMenuItem";
            this.keysToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.keysToolStripMenuItem.Text = "Keys";
            this.keysToolStripMenuItem.Click += new System.EventHandler(this.keysToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.tb_originalText);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2.Controls.Add(this.toolStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(787, 398);
            this.splitContainer1.SplitterDistance = 188;
            this.splitContainer1.TabIndex = 1;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lb_imageCounter);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.lb_Translator2Index);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.lb_Translator2Key);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lb_State);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lb_RunKey);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tb_refreshTime);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lb_TranslatorIndex);
            this.groupBox1.Controls.Add(this.lb_TranslatorKey);
            this.groupBox1.Controls.Add(this.lb_AreaKey);
            this.groupBox1.Controls.Add(this.lb_AreaIndex);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(781, 69);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "info";
            // 
            // lb_imageCounter
            // 
            this.lb_imageCounter.AutoSize = true;
            this.lb_imageCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.lb_imageCounter.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_imageCounter.Location = new System.Drawing.Point(301, 48);
            this.lb_imageCounter.Name = "lb_imageCounter";
            this.lb_imageCounter.Size = new System.Drawing.Size(35, 13);
            this.lb_imageCounter.TabIndex = 20;
            this.lb_imageCounter.Text = "label3";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label12.Location = new System.Drawing.Point(230, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Img count:";
            // 
            // lb_Translator2Index
            // 
            this.lb_Translator2Index.AutoSize = true;
            this.lb_Translator2Index.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.lb_Translator2Index.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_Translator2Index.Location = new System.Drawing.Point(126, 48);
            this.lb_Translator2Index.Name = "lb_Translator2Index";
            this.lb_Translator2Index.Size = new System.Drawing.Size(35, 13);
            this.lb_Translator2Index.TabIndex = 18;
            this.lb_Translator2Index.Text = "label8";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label11.Location = new System.Drawing.Point(9, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "2nd translator index:";
            // 
            // lb_Translator2Key
            // 
            this.lb_Translator2Key.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_Translator2Key.AutoSize = true;
            this.lb_Translator2Key.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.lb_Translator2Key.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_Translator2Key.Location = new System.Drawing.Point(728, 48);
            this.lb_Translator2Key.Name = "lb_Translator2Key";
            this.lb_Translator2Key.Size = new System.Drawing.Size(35, 13);
            this.lb_Translator2Key.TabIndex = 16;
            this.lb_Translator2Key.Text = "label4";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label9.Location = new System.Drawing.Point(629, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "2nd translator key:";
            // 
            // lb_State
            // 
            this.lb_State.AutoSize = true;
            this.lb_State.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.lb_State.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_State.Location = new System.Drawing.Point(301, 31);
            this.lb_State.Name = "lb_State";
            this.lb_State.Size = new System.Drawing.Size(35, 13);
            this.lb_State.TabIndex = 14;
            this.lb_State.Text = "label3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label10.Location = new System.Drawing.Point(230, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "State:";
            // 
            // lb_RunKey
            // 
            this.lb_RunKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_RunKey.AutoSize = true;
            this.lb_RunKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.lb_RunKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_RunKey.Location = new System.Drawing.Point(573, 14);
            this.lb_RunKey.Name = "lb_RunKey";
            this.lb_RunKey.Size = new System.Drawing.Size(35, 13);
            this.lb_RunKey.TabIndex = 12;
            this.lb_RunKey.Text = "label3";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label8.Location = new System.Drawing.Point(516, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Run key:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(360, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "sec.";
            // 
            // tb_refreshTime
            // 
            this.tb_refreshTime.DecimalPlaces = 2;
            this.tb_refreshTime.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.tb_refreshTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.tb_refreshTime.Location = new System.Drawing.Point(304, 11);
            this.tb_refreshTime.Name = "tb_refreshTime";
            this.tb_refreshTime.Size = new System.Drawing.Size(50, 20);
            this.tb_refreshTime.TabIndex = 9;
            this.tb_refreshTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(230, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Refresh time:";
            // 
            // lb_TranslatorIndex
            // 
            this.lb_TranslatorIndex.AutoSize = true;
            this.lb_TranslatorIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.lb_TranslatorIndex.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_TranslatorIndex.Location = new System.Drawing.Point(126, 31);
            this.lb_TranslatorIndex.Name = "lb_TranslatorIndex";
            this.lb_TranslatorIndex.Size = new System.Drawing.Size(35, 13);
            this.lb_TranslatorIndex.TabIndex = 7;
            this.lb_TranslatorIndex.Text = "label8";
            // 
            // lb_TranslatorKey
            // 
            this.lb_TranslatorKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_TranslatorKey.AutoSize = true;
            this.lb_TranslatorKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.lb_TranslatorKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_TranslatorKey.Location = new System.Drawing.Point(728, 31);
            this.lb_TranslatorKey.Name = "lb_TranslatorKey";
            this.lb_TranslatorKey.Size = new System.Drawing.Size(35, 13);
            this.lb_TranslatorKey.TabIndex = 3;
            this.lb_TranslatorKey.Text = "label4";
            // 
            // lb_AreaKey
            // 
            this.lb_AreaKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_AreaKey.AutoSize = true;
            this.lb_AreaKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.lb_AreaKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_AreaKey.Location = new System.Drawing.Point(728, 14);
            this.lb_AreaKey.Name = "lb_AreaKey";
            this.lb_AreaKey.Size = new System.Drawing.Size(35, 13);
            this.lb_AreaKey.TabIndex = 2;
            this.lb_AreaKey.Text = "label3";
            // 
            // lb_AreaIndex
            // 
            this.lb_AreaIndex.AutoSize = true;
            this.lb_AreaIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.lb_AreaIndex.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_AreaIndex.Location = new System.Drawing.Point(126, 14);
            this.lb_AreaIndex.Name = "lb_AreaIndex";
            this.lb_AreaIndex.Size = new System.Drawing.Size(35, 13);
            this.lb_AreaIndex.TabIndex = 6;
            this.lb_AreaIndex.Text = "label7";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(25, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Translator index:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(648, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Translator key:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(671, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Area key:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(48, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Area index:";
            // 
            // tb_originalText
            // 
            this.tb_originalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_originalText.BackColor = System.Drawing.SystemColors.Window;
            this.tb_originalText.Location = new System.Drawing.Point(3, 77);
            this.tb_originalText.Multiline = true;
            this.tb_originalText.Name = "tb_originalText";
            this.tb_originalText.ReadOnly = true;
            this.tb_originalText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_originalText.Size = new System.Drawing.Size(781, 108);
            this.tb_originalText.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 25);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tb_translatedText);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tb_translatedText2);
            this.splitContainer2.Size = new System.Drawing.Size(787, 181);
            this.splitContainer2.SplitterDistance = 91;
            this.splitContainer2.SplitterWidth = 2;
            this.splitContainer2.TabIndex = 2;
            // 
            // tb_translatedText
            // 
            this.tb_translatedText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_translatedText.BackColor = System.Drawing.SystemColors.Window;
            this.tb_translatedText.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tb_translatedText.Location = new System.Drawing.Point(3, 0);
            this.tb_translatedText.Multiline = true;
            this.tb_translatedText.Name = "tb_translatedText";
            this.tb_translatedText.ReadOnly = true;
            this.tb_translatedText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_translatedText.Size = new System.Drawing.Size(781, 88);
            this.tb_translatedText.TabIndex = 0;
            // 
            // tb_translatedText2
            // 
            this.tb_translatedText2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_translatedText2.BackColor = System.Drawing.SystemColors.Window;
            this.tb_translatedText2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tb_translatedText2.Location = new System.Drawing.Point(3, 2);
            this.tb_translatedText2.Multiline = true;
            this.tb_translatedText2.Name = "tb_translatedText2";
            this.tb_translatedText2.ReadOnly = true;
            this.tb_translatedText2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_translatedText2.Size = new System.Drawing.Size(781, 83);
            this.tb_translatedText2.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.bt_target_font,
            this.bt_target_color,
            this.bt_target_bgColor});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(787, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(0, 22);
            // 
            // bt_target_font
            // 
            this.bt_target_font.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_target_font.Name = "bt_target_font";
            this.bt_target_font.Size = new System.Drawing.Size(23, 22);
            this.bt_target_font.Text = "A";
            this.bt_target_font.Click += new System.EventHandler(this.bt_target_font_Click);
            // 
            // bt_target_color
            // 
            this.bt_target_color.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_target_color.Name = "bt_target_color";
            this.bt_target_color.Size = new System.Drawing.Size(23, 22);
            this.bt_target_color.Tag = "";
            this.bt_target_color.Text = "F";
            this.bt_target_color.Click += new System.EventHandler(this.bt_target_color_Click);
            // 
            // bt_target_bgColor
            // 
            this.bt_target_bgColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_target_bgColor.Name = "bt_target_bgColor";
            this.bt_target_bgColor.Size = new System.Drawing.Size(23, 22);
            this.bt_target_bgColor.Text = "B";
            this.bt_target_bgColor.Click += new System.EventHandler(this.bt_target_bgColor_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(787, 422);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Visual Novel Translator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb_refreshTime)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown tb_refreshTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lb_TranslatorIndex;
        private System.Windows.Forms.Label lb_TranslatorKey;
        private System.Windows.Forms.Label lb_AreaKey;
        private System.Windows.Forms.Label lb_AreaIndex;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_originalText;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton bt_target_color;
        private System.Windows.Forms.TextBox tb_translatedText;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem areasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem keysToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label lb_State;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lb_RunKey;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripButton bt_target_font;
        private System.Windows.Forms.ToolStripButton bt_target_bgColor;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox tb_translatedText2;
        private System.Windows.Forms.Label lb_Translator2Key;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lb_Translator2Index;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lb_imageCounter;
        private System.Windows.Forms.Label label12;
    }
}

