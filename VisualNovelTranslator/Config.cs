﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualNovelTranslator
{
    public static class Config
    {
        public static Settings settings = new Settings();

        public static AreaSettings currentAreaSettings
        {
            get
            {
                if (settings.currentArea < 0 || settings.currentArea >= settings.areaSettings.Count)
                    return null;
                return settings.areaSettings[settings.currentArea];
            }
        }

        [Serializable]
        public class AreaSettings
        {
            public Rectangle boundsP;
            public bool thresholdEnabled;
            public int thresholdValue;
            public int thresholdMethod;
            public bool thresholdInverted;
        }

        [Serializable]
        public class KeysSettings
        {
            public Keys keyRun = Keys.D1;
            public Keys keyChangeArea = Keys.D2;
            public Keys keyChangeTranslator = Keys.D3;
            public Keys keyChangeTranslator2 = Keys.D4;
            public Keys keyUseTranslator2 = Keys.D5;
            public Keys keyFontSizeUp = Keys.D6;
            public Keys keyFontSizeDown = Keys.D7;
        }

        [Serializable]
        public class Settings
        {
            public List<AreaSettings> areaSettings = new List<AreaSettings>();
            public KeysSettings keysSettings = new KeysSettings();
            public int currentArea = 0;
            public int currentTranslator = 0;
            public int currentTranslator2 = 0;
            public bool useTranslator2;
            public Font textFont;
            public Color textColor;
            public Color backgroundColor;
            public float splitPosition;
            public bool initializedSettings = false;
        }

        public static void LoadFile(string filename)
        {
            try
            {
                using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    settings = (Settings)formatter.Deserialize(stream);
                }
            }
            catch { }
        }

        public static void SaveFile(string filename)
        {
            using (FileStream stream = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, settings);
            }
        }
    }
}
