﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualNovelTranslator
{
    public partial class Form1 : Form
    {
        private const string configFile = "config.vncfg";
        private const int numTranslators = 5;
        private InterceptKeys.Listener keysListener;

        private bool _runWorker;
        private int _imageCounter;

        public Form1()
        {
            InitializeComponent();
            Config.LoadFile(configFile);
            InterceptKeys.Start();
            keysListener = InterceptKeys.GetListener();
            keysListener.onKeyDown += KeysListener_onKeyDown;
            backgroundWorker1.RunWorkerAsync();
            UpdateLabels();
            LoadLayoutForm();
        }

        private void KeysListener_onKeyDown(Keys key)
        {
            var configKeys = Config.settings.keysSettings;

            if (key == configKeys.keyRun)
            {
                _runWorker = !_runWorker;
                UpdateLabels();
            }
            else if (key == configKeys.keyChangeArea)
            {
                if (Config.settings.areaSettings.Count > 0)
                    Config.settings.currentArea = (Config.settings.currentArea + 1) % Config.settings.areaSettings.Count;
                
                UpdateLabels();
                Config.SaveFile(configFile);
            }
            else if (key == configKeys.keyChangeTranslator)
            {
                Config.settings.currentTranslator = (Config.settings.currentTranslator + 1) % numTranslators;
                UpdateLabels();
                Config.SaveFile(configFile);
            }
            else if (key == configKeys.keyChangeTranslator2)
            {
                Config.settings.currentTranslator2 = (Config.settings.currentTranslator2 + 1) % numTranslators;
                UpdateLabels();
                Config.SaveFile(configFile);
            }
            else if (key == configKeys.keyUseTranslator2)
            {
                splitContainer2.Panel2Collapsed = Config.settings.useTranslator2;
                SaveLayoutForm();
            }
            else if (key == configKeys.keyFontSizeUp)
            {
                SetFontSize(tb_translatedText.Font.Size + 2);
            }
            else if (key == configKeys.keyFontSizeDown)
            {
                SetFontSize(tb_translatedText.Font.Size - 2);
            }
        }

        private void UpdateLabels()
        {
            lb_AreaIndex.Text = Config.settings.currentArea.ToString();
            lb_TranslatorIndex.Text = Config.settings.currentTranslator.ToString();
            lb_Translator2Index.Text = Config.settings.currentTranslator2.ToString();
            lb_AreaKey.Text = Config.settings.keysSettings.keyChangeArea.ToString();
            lb_TranslatorKey.Text = Config.settings.keysSettings.keyChangeTranslator.ToString();
            lb_Translator2Key.Text = Config.settings.keysSettings.keyChangeTranslator2.ToString();
            lb_RunKey.Text = Config.settings.keysSettings.keyRun.ToString();
            lb_State.Text = _runWorker ? "Running" : "Stopped";
            lb_imageCounter.Text = _imageCounter.ToString();
        }

        private void openConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                Config.LoadFile(dlg.FileName);
                Config.settings.currentArea = 0;
            }
        }

        private void saveConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new SaveFileDialog();
            dlg.DefaultExt = "vncfg";
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                Config.SaveFile(dlg.FileName);
            }
        }

        private void areasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _runWorker = false;
            keysListener.Stop();

            var dlg = new SettingsArea();
            dlg.ShowDialog(this);

            keysListener.Start();

            Config.SaveFile(configFile);
            UpdateLabels();
        }

        private void keysToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _runWorker = false;
            keysListener.Stop();

            var dlg = new SettingsKeys();
            dlg.ShowDialog(this);

            keysListener.Start();

            Config.SaveFile(configFile);
            UpdateLabels();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var screen = new ScreenCapture();
            string previousText = null;
            string previousTranslated = null;
            int previousTranslator = -1;
            DateTime before, after;
            DebugTimer timer = new DebugTimer(true);
            Task<string> taskTranslator, taskTranslator2;
            while (true)
            {
                before = DateTime.UtcNow;
                var area = Config.currentAreaSettings;
                if (_runWorker && area != null)
                {
                    timer.Restart();
                    var image = screen.CaptureScreen();
                    timer.LogR("Capture screen");
                    image = Common.CropImageP(image, area.boundsP);
                    timer.LogR("Crop image");
                    if (area.thresholdEnabled)
                    {
                        image = Common.ThresholdImageNew(image, (byte)area.thresholdValue);
                        timer.LogR("Threshold image");
                    }
                    var text = Common.ImageToText(image);
                    timer.LogR("OCR");
                    if (text != previousText || previousTranslated == "ERROR" || previousTranslator != Config.settings.currentTranslator)
                    {
                        previousTranslator = Config.settings.currentTranslator;
                        previousText = text;
                        taskTranslator = Task.Run(() => TranslateText(text, Config.settings.currentTranslator));
                        string translated2 = null;
                        if (Config.settings.useTranslator2)
                        {
                            translated2 = TranslateText(text, Config.settings.currentTranslator2);
                        }
                        taskTranslator.Wait();
                        var translated = taskTranslator.Result;
                        previousTranslated = translated;
                        timer.LogR("Translator");
                        this.Invoke((MethodInvoker)(() => {
                            tb_originalText.Text = text;
                            tb_translatedText.Text = translated;
                            _imageCounter++;
                            lb_imageCounter.Text = _imageCounter.ToString();
                            if (Config.settings.useTranslator2)
                            {
                                tb_translatedText2.Text = translated2;
                            }
                        }));
                        timer.LogR("Invoke");
                    }
                }
                after = DateTime.UtcNow;
                var elapsed = after - before;
                var diff = (double)(tb_refreshTime.Value * 1000) - elapsed.TotalMilliseconds;
                if (diff > 0)
                    System.Threading.Thread.Sleep((int)diff);
            }
        }

        private string TranslateText(string text, int method)
        {
            switch (method)
            {
                case 0:
                    return Common.TranslateWithGoogle(text, "en", "es");
                case 1:
                case 2:
                case 3:
                case 4:
                    return Common.TranslateWithDeepl(text, "EN", "ES", method - 1);
                default:
                    return "Translator not supported";
            }
        }

        private void SetFontSize(float size)
        {
            var font = tb_translatedText.Font;
            font = new Font(font.FontFamily, size);
            tb_translatedText.Font = font;
            tb_translatedText2.Font = font;
            SaveLayoutForm();
        }

        private void bt_target_font_Click(object sender, EventArgs e)
        {
            var dlg = new FontDialog();
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                tb_translatedText.Font = dlg.Font;
                tb_translatedText2.Font = dlg.Font;
                SaveLayoutForm();
            }
        }

        private void bt_target_color_Click(object sender, EventArgs e)
        {
            var dlg = new ColorDialog();
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                tb_translatedText.ForeColor = dlg.Color;
                tb_translatedText2.ForeColor = dlg.Color;
                SaveLayoutForm();
            }
        }

        private void bt_target_bgColor_Click(object sender, EventArgs e)
        {
            var dlg = new ColorDialog();
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                tb_translatedText.BackColor = dlg.Color;
                tb_translatedText2.BackColor = dlg.Color;
                SaveLayoutForm();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            InterceptKeys.Stop();
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            SaveLayoutForm();
        }

        private void SaveLayoutForm()
        {
            if (_loadingLayout)
                return;

            Config.settings.initializedSettings = true;
            Config.settings.splitPosition = (float)splitContainer1.SplitterDistance / splitContainer1.Size.Height;
            Config.settings.textColor = tb_translatedText.ForeColor;
            Config.settings.backgroundColor = tb_translatedText.BackColor;
            Config.settings.textFont = tb_translatedText.Font;
            Config.settings.useTranslator2 = !splitContainer2.Panel2Collapsed;
            Config.SaveFile(configFile);
        }

        bool _loadingLayout;

        private void LoadLayoutForm()
        {
            if (!Config.settings.initializedSettings)
                return;

            _loadingLayout = true;

            splitContainer1.SplitterDistance = (int)(Config.settings.splitPosition * splitContainer1.Size.Height);
            tb_translatedText.ForeColor = Config.settings.textColor;
            tb_translatedText.BackColor = Config.settings.backgroundColor;
            tb_translatedText.Font = Config.settings.textFont;
            tb_translatedText2.ForeColor = Config.settings.textColor;
            tb_translatedText2.BackColor = Config.settings.backgroundColor;
            tb_translatedText2.Font = Config.settings.textFont;
            splitContainer2.Panel2Collapsed = !Config.settings.useTranslator2;

            _loadingLayout = false;
        }
    }
}
