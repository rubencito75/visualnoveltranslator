﻿namespace VisualNovelTranslator
{
    partial class SettingsArea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_test = new System.Windows.Forms.Button();
            this.tb_result = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cb_thresholdInverted = new System.Windows.Forms.CheckBox();
            this.cb_thresholdMethod = new System.Windows.Forms.ComboBox();
            this.cb_thresholdEnabled = new System.Windows.Forms.CheckBox();
            this.tb_threshold = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_cropH = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_cropX = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_cropW = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_cropY = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bt_delete = new System.Windows.Forms.Button();
            this.bt_save = new System.Windows.Forms.Button();
            this.bt_create = new System.Windows.Forms.Button();
            this.cb_areaIndex = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb_threshold)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb_cropH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_cropX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_cropW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_cropY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bt_test
            // 
            this.bt_test.Location = new System.Drawing.Point(23, 151);
            this.bt_test.Name = "bt_test";
            this.bt_test.Size = new System.Drawing.Size(75, 35);
            this.bt_test.TabIndex = 13;
            this.bt_test.Text = "Test";
            this.bt_test.UseVisualStyleBackColor = true;
            this.bt_test.Click += new System.EventHandler(this.bt_test_Click);
            // 
            // tb_result
            // 
            this.tb_result.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_result.BackColor = System.Drawing.SystemColors.Window;
            this.tb_result.Location = new System.Drawing.Point(23, 354);
            this.tb_result.Multiline = true;
            this.tb_result.Name = "tb_result";
            this.tb_result.Size = new System.Drawing.Size(756, 76);
            this.tb_result.TabIndex = 11;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cb_thresholdInverted);
            this.groupBox2.Controls.Add(this.cb_thresholdMethod);
            this.groupBox2.Controls.Add(this.cb_thresholdEnabled);
            this.groupBox2.Controls.Add(this.tb_threshold);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(271, 112);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(301, 79);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Threshold";
            // 
            // cb_thresholdInverted
            // 
            this.cb_thresholdInverted.AutoSize = true;
            this.cb_thresholdInverted.Location = new System.Drawing.Point(137, 21);
            this.cb_thresholdInverted.Name = "cb_thresholdInverted";
            this.cb_thresholdInverted.Size = new System.Drawing.Size(65, 17);
            this.cb_thresholdInverted.TabIndex = 13;
            this.cb_thresholdInverted.Text = "Inverted";
            this.cb_thresholdInverted.UseVisualStyleBackColor = true;
            this.cb_thresholdInverted.CheckedChanged += new System.EventHandler(this.componentChanged);
            // 
            // cb_thresholdMethod
            // 
            this.cb_thresholdMethod.FormattingEnabled = true;
            this.cb_thresholdMethod.Items.AddRange(new object[] {
            "Lightness",
            "Average",
            "Luminosity"});
            this.cb_thresholdMethod.Location = new System.Drawing.Point(137, 47);
            this.cb_thresholdMethod.Name = "cb_thresholdMethod";
            this.cb_thresholdMethod.Size = new System.Drawing.Size(121, 21);
            this.cb_thresholdMethod.TabIndex = 12;
            this.cb_thresholdMethod.SelectedIndexChanged += new System.EventHandler(this.componentChanged);
            // 
            // cb_thresholdEnabled
            // 
            this.cb_thresholdEnabled.AutoSize = true;
            this.cb_thresholdEnabled.Location = new System.Drawing.Point(21, 21);
            this.cb_thresholdEnabled.Name = "cb_thresholdEnabled";
            this.cb_thresholdEnabled.Size = new System.Drawing.Size(64, 17);
            this.cb_thresholdEnabled.TabIndex = 11;
            this.cb_thresholdEnabled.Text = "enabled";
            this.cb_thresholdEnabled.UseVisualStyleBackColor = true;
            this.cb_thresholdEnabled.CheckedChanged += new System.EventHandler(this.componentChanged);
            // 
            // tb_threshold
            // 
            this.tb_threshold.Location = new System.Drawing.Point(67, 48);
            this.tb_threshold.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.tb_threshold.Name = "tb_threshold";
            this.tb_threshold.Size = new System.Drawing.Size(54, 20);
            this.tb_threshold.TabIndex = 9;
            this.tb_threshold.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.tb_threshold.ValueChanged += new System.EventHandler(this.componentChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Value";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_cropH);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tb_cropX);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tb_cropW);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tb_cropY);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(271, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(207, 79);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Crop";
            // 
            // tb_cropH
            // 
            this.tb_cropH.Location = new System.Drawing.Point(135, 47);
            this.tb_cropH.Name = "tb_cropH";
            this.tb_cropH.Size = new System.Drawing.Size(53, 20);
            this.tb_cropH.TabIndex = 7;
            this.tb_cropH.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.tb_cropH.ValueChanged += new System.EventHandler(this.componentChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(114, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "H";
            // 
            // tb_cropX
            // 
            this.tb_cropX.Location = new System.Drawing.Point(32, 20);
            this.tb_cropX.Name = "tb_cropX";
            this.tb_cropX.Size = new System.Drawing.Size(54, 20);
            this.tb_cropX.TabIndex = 0;
            this.tb_cropX.ValueChanged += new System.EventHandler(this.componentChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "X";
            // 
            // tb_cropW
            // 
            this.tb_cropW.Location = new System.Drawing.Point(33, 46);
            this.tb_cropW.Name = "tb_cropW";
            this.tb_cropW.Size = new System.Drawing.Size(53, 20);
            this.tb_cropW.TabIndex = 5;
            this.tb_cropW.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.tb_cropW.ValueChanged += new System.EventHandler(this.componentChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "W";
            // 
            // tb_cropY
            // 
            this.tb_cropY.Location = new System.Drawing.Point(135, 20);
            this.tb_cropY.Name = "tb_cropY";
            this.tb_cropY.Size = new System.Drawing.Size(53, 20);
            this.tb_cropY.TabIndex = 3;
            this.tb_cropY.ValueChanged += new System.EventHandler(this.componentChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(114, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Y";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(23, 197);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(757, 151);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bt_delete);
            this.groupBox3.Controls.Add(this.bt_save);
            this.groupBox3.Controls.Add(this.bt_create);
            this.groupBox3.Controls.Add(this.cb_areaIndex);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(23, 27);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(208, 111);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Areas";
            // 
            // bt_delete
            // 
            this.bt_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_delete.Location = new System.Drawing.Point(140, 75);
            this.bt_delete.Name = "bt_delete";
            this.bt_delete.Size = new System.Drawing.Size(62, 23);
            this.bt_delete.TabIndex = 4;
            this.bt_delete.Text = "Delete";
            this.bt_delete.UseVisualStyleBackColor = true;
            this.bt_delete.Click += new System.EventHandler(this.bt_delete_Click);
            // 
            // bt_save
            // 
            this.bt_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_save.Location = new System.Drawing.Point(72, 75);
            this.bt_save.Name = "bt_save";
            this.bt_save.Size = new System.Drawing.Size(62, 23);
            this.bt_save.TabIndex = 3;
            this.bt_save.Text = "Save";
            this.bt_save.UseVisualStyleBackColor = true;
            this.bt_save.Click += new System.EventHandler(this.bt_save_Click);
            // 
            // bt_create
            // 
            this.bt_create.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_create.Location = new System.Drawing.Point(4, 75);
            this.bt_create.Name = "bt_create";
            this.bt_create.Size = new System.Drawing.Size(62, 23);
            this.bt_create.TabIndex = 2;
            this.bt_create.Text = "Create";
            this.bt_create.UseVisualStyleBackColor = true;
            this.bt_create.Click += new System.EventHandler(this.bt_create_Click);
            // 
            // cb_areaIndex
            // 
            this.cb_areaIndex.FormattingEnabled = true;
            this.cb_areaIndex.Location = new System.Drawing.Point(58, 30);
            this.cb_areaIndex.Name = "cb_areaIndex";
            this.cb_areaIndex.Size = new System.Drawing.Size(121, 21);
            this.cb_areaIndex.TabIndex = 1;
            this.cb_areaIndex.SelectedIndexChanged += new System.EventHandler(this.cb_areaIndex_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Index:";
            // 
            // SettingsArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.bt_test);
            this.Controls.Add(this.tb_result);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "SettingsArea";
            this.Text = "SettingsArea";
            this.Load += new System.EventHandler(this.SettingsArea_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb_threshold)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb_cropH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_cropX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_cropW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_cropY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_test;
        private System.Windows.Forms.TextBox tb_result;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cb_thresholdInverted;
        private System.Windows.Forms.ComboBox cb_thresholdMethod;
        private System.Windows.Forms.CheckBox cb_thresholdEnabled;
        private System.Windows.Forms.NumericUpDown tb_threshold;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown tb_cropH;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown tb_cropX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown tb_cropW;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown tb_cropY;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bt_save;
        private System.Windows.Forms.Button bt_create;
        private System.Windows.Forms.ComboBox cb_areaIndex;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bt_delete;
    }
}