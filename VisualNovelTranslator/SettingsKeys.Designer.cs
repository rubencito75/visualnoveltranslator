﻿namespace VisualNovelTranslator
{
    partial class SettingsKeys
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lb_run = new System.Windows.Forms.Label();
            this.bt_run = new System.Windows.Forms.Button();
            this.bt_setArea = new System.Windows.Forms.Button();
            this.lb_setArea = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bt_setTranslator = new System.Windows.Forms.Button();
            this.lb_setTranslator = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.bt_setTranslator2 = new System.Windows.Forms.Button();
            this.lb_setTranslator2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bt_useTranslator2 = new System.Windows.Forms.Button();
            this.lb_useTranslator2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.bt_fontSizeUp = new System.Windows.Forms.Button();
            this.lb_fontSizeUp = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bt_fontSizeDown = new System.Windows.Forms.Button();
            this.lb_fontSizeDown = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(107, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Start / Stop:";
            // 
            // lb_run
            // 
            this.lb_run.AutoSize = true;
            this.lb_run.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_run.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_run.Location = new System.Drawing.Point(230, 50);
            this.lb_run.Name = "lb_run";
            this.lb_run.Size = new System.Drawing.Size(41, 15);
            this.lb_run.TabIndex = 1;
            this.lb_run.Text = "label2";
            // 
            // bt_run
            // 
            this.bt_run.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_run.Location = new System.Drawing.Point(36, 46);
            this.bt_run.Name = "bt_run";
            this.bt_run.Size = new System.Drawing.Size(46, 23);
            this.bt_run.TabIndex = 2;
            this.bt_run.Text = "set";
            this.bt_run.UseVisualStyleBackColor = true;
            this.bt_run.Click += new System.EventHandler(this.bt_run_Click);
            // 
            // bt_setArea
            // 
            this.bt_setArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_setArea.Location = new System.Drawing.Point(36, 86);
            this.bt_setArea.Name = "bt_setArea";
            this.bt_setArea.Size = new System.Drawing.Size(46, 23);
            this.bt_setArea.TabIndex = 5;
            this.bt_setArea.Text = "set";
            this.bt_setArea.UseVisualStyleBackColor = true;
            this.bt_setArea.Click += new System.EventHandler(this.bt_setArea_Click);
            // 
            // lb_setArea
            // 
            this.lb_setArea.AutoSize = true;
            this.lb_setArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_setArea.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_setArea.Location = new System.Drawing.Point(230, 89);
            this.lb_setArea.Name = "lb_setArea";
            this.lb_setArea.Size = new System.Drawing.Size(41, 15);
            this.lb_setArea.TabIndex = 4;
            this.lb_setArea.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(107, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Change area:";
            // 
            // bt_setTranslator
            // 
            this.bt_setTranslator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_setTranslator.Location = new System.Drawing.Point(36, 125);
            this.bt_setTranslator.Name = "bt_setTranslator";
            this.bt_setTranslator.Size = new System.Drawing.Size(46, 23);
            this.bt_setTranslator.TabIndex = 8;
            this.bt_setTranslator.Text = "set";
            this.bt_setTranslator.UseVisualStyleBackColor = true;
            this.bt_setTranslator.Click += new System.EventHandler(this.bt_setTranslator_Click);
            // 
            // lb_setTranslator
            // 
            this.lb_setTranslator.AutoSize = true;
            this.lb_setTranslator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_setTranslator.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_setTranslator.Location = new System.Drawing.Point(230, 129);
            this.lb_setTranslator.Name = "lb_setTranslator";
            this.lb_setTranslator.Size = new System.Drawing.Size(41, 15);
            this.lb_setTranslator.TabIndex = 7;
            this.lb_setTranslator.Text = "label5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(107, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "Change translator:";
            // 
            // bt_setTranslator2
            // 
            this.bt_setTranslator2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_setTranslator2.Location = new System.Drawing.Point(36, 163);
            this.bt_setTranslator2.Name = "bt_setTranslator2";
            this.bt_setTranslator2.Size = new System.Drawing.Size(46, 23);
            this.bt_setTranslator2.TabIndex = 11;
            this.bt_setTranslator2.Text = "set";
            this.bt_setTranslator2.UseVisualStyleBackColor = true;
            this.bt_setTranslator2.Click += new System.EventHandler(this.bt_setTranslator2_Click);
            // 
            // lb_setTranslator2
            // 
            this.lb_setTranslator2.AutoSize = true;
            this.lb_setTranslator2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_setTranslator2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_setTranslator2.Location = new System.Drawing.Point(230, 167);
            this.lb_setTranslator2.Name = "lb_setTranslator2";
            this.lb_setTranslator2.Size = new System.Drawing.Size(41, 15);
            this.lb_setTranslator2.TabIndex = 10;
            this.lb_setTranslator2.Text = "label5";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(107, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Change translator 2:";
            // 
            // bt_useTranslator2
            // 
            this.bt_useTranslator2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_useTranslator2.Location = new System.Drawing.Point(36, 203);
            this.bt_useTranslator2.Name = "bt_useTranslator2";
            this.bt_useTranslator2.Size = new System.Drawing.Size(46, 23);
            this.bt_useTranslator2.TabIndex = 14;
            this.bt_useTranslator2.Text = "set";
            this.bt_useTranslator2.UseVisualStyleBackColor = true;
            this.bt_useTranslator2.Click += new System.EventHandler(this.bt_useTranslator2_Click);
            // 
            // lb_useTranslator2
            // 
            this.lb_useTranslator2.AutoSize = true;
            this.lb_useTranslator2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_useTranslator2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_useTranslator2.Location = new System.Drawing.Point(230, 207);
            this.lb_useTranslator2.Name = "lb_useTranslator2";
            this.lb_useTranslator2.Size = new System.Drawing.Size(41, 15);
            this.lb_useTranslator2.TabIndex = 13;
            this.lb_useTranslator2.Text = "label5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label7.Location = new System.Drawing.Point(107, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "Use 2nd translator:";
            // 
            // bt_fontSizeUp
            // 
            this.bt_fontSizeUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_fontSizeUp.Location = new System.Drawing.Point(36, 245);
            this.bt_fontSizeUp.Name = "bt_fontSizeUp";
            this.bt_fontSizeUp.Size = new System.Drawing.Size(46, 23);
            this.bt_fontSizeUp.TabIndex = 17;
            this.bt_fontSizeUp.Text = "set";
            this.bt_fontSizeUp.UseVisualStyleBackColor = true;
            this.bt_fontSizeUp.Click += new System.EventHandler(this.bt_fontSizeUp_Click);
            // 
            // lb_fontSizeUp
            // 
            this.lb_fontSizeUp.AutoSize = true;
            this.lb_fontSizeUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_fontSizeUp.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_fontSizeUp.Location = new System.Drawing.Point(230, 249);
            this.lb_fontSizeUp.Name = "lb_fontSizeUp";
            this.lb_fontSizeUp.Size = new System.Drawing.Size(41, 15);
            this.lb_fontSizeUp.TabIndex = 16;
            this.lb_fontSizeUp.Text = "label5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(107, 249);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 15);
            this.label5.TabIndex = 15;
            this.label5.Text = "Font size up:";
            // 
            // bt_fontSizeDown
            // 
            this.bt_fontSizeDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_fontSizeDown.Location = new System.Drawing.Point(36, 288);
            this.bt_fontSizeDown.Name = "bt_fontSizeDown";
            this.bt_fontSizeDown.Size = new System.Drawing.Size(46, 23);
            this.bt_fontSizeDown.TabIndex = 20;
            this.bt_fontSizeDown.Text = "set";
            this.bt_fontSizeDown.UseVisualStyleBackColor = true;
            this.bt_fontSizeDown.Click += new System.EventHandler(this.bt_fontSizeDown_Click);
            // 
            // lb_fontSizeDown
            // 
            this.lb_fontSizeDown.AutoSize = true;
            this.lb_fontSizeDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_fontSizeDown.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_fontSizeDown.Location = new System.Drawing.Point(230, 292);
            this.lb_fontSizeDown.Name = "lb_fontSizeDown";
            this.lb_fontSizeDown.Size = new System.Drawing.Size(41, 15);
            this.lb_fontSizeDown.TabIndex = 19;
            this.lb_fontSizeDown.Text = "label5";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label9.Location = new System.Drawing.Point(107, 292);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 15);
            this.label9.TabIndex = 18;
            this.label9.Text = "Font size down:";
            // 
            // SettingsKeys
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 338);
            this.Controls.Add(this.bt_fontSizeDown);
            this.Controls.Add(this.lb_fontSizeDown);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.bt_fontSizeUp);
            this.Controls.Add(this.lb_fontSizeUp);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bt_useTranslator2);
            this.Controls.Add(this.lb_useTranslator2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.bt_setTranslator2);
            this.Controls.Add(this.lb_setTranslator2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bt_setTranslator);
            this.Controls.Add(this.lb_setTranslator);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bt_setArea);
            this.Controls.Add(this.lb_setArea);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.bt_run);
            this.Controls.Add(this.lb_run);
            this.Controls.Add(this.label1);
            this.Name = "SettingsKeys";
            this.Text = "SettingsKeys";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsKeys_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_run;
        private System.Windows.Forms.Button bt_run;
        private System.Windows.Forms.Button bt_setArea;
        private System.Windows.Forms.Label lb_setArea;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button bt_setTranslator;
        private System.Windows.Forms.Label lb_setTranslator;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bt_setTranslator2;
        private System.Windows.Forms.Label lb_setTranslator2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bt_useTranslator2;
        private System.Windows.Forms.Label lb_useTranslator2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button bt_fontSizeUp;
        private System.Windows.Forms.Label lb_fontSizeUp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bt_fontSizeDown;
        private System.Windows.Forms.Label lb_fontSizeDown;
        private System.Windows.Forms.Label label9;
    }
}