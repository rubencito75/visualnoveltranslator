﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualNovelTranslator
{
    public partial class SettingsKeys : Form
    {
        private List<Button> _listButtons;
        private string keyWaiting;
        private InterceptKeys.Listener keysListener;

        public SettingsKeys()
        {
            InitializeComponent();
            _listButtons = new List<Button>()
            {
                bt_run, bt_setArea, bt_setTranslator, bt_setTranslator2, bt_useTranslator2, bt_fontSizeUp, bt_fontSizeDown
            };
            keysListener = InterceptKeys.GetListener();
            keysListener.onKeyDown += KeysListener_onKeyDown;
            UpdateLabels();
        }

        private void KeysListener_onKeyDown(Keys key)
        {
            if (keyWaiting != null)
            {
                if (key != Keys.Escape && key != Keys.Back)
                {
                    typeof(Config.KeysSettings).GetField(keyWaiting)
                        .SetValue(Config.settings.keysSettings, key);
                }

                foreach (var bt in _listButtons)
                    bt.Enabled = true;

                keyWaiting = null;
                UpdateLabels();
            }
        }

        private void UpdateLabels()
        {
            lb_run.Text = Config.settings.keysSettings.keyRun.ToString();
            lb_setArea.Text = Config.settings.keysSettings.keyChangeArea.ToString();
            lb_setTranslator.Text = Config.settings.keysSettings.keyChangeTranslator.ToString();
            lb_setTranslator2.Text = Config.settings.keysSettings.keyChangeTranslator2.ToString();
            lb_useTranslator2.Text = Config.settings.keysSettings.keyUseTranslator2.ToString();
            lb_fontSizeUp.Text = Config.settings.keysSettings.keyFontSizeUp.ToString();
            lb_fontSizeDown.Text = Config.settings.keysSettings.keyFontSizeDown.ToString();
        }

        private void SettingsKeys_FormClosing(object sender, FormClosingEventArgs e)
        {
            InterceptKeys.DestroyListener(keysListener);
        }

        private void bt_run_Click(object sender, EventArgs e)
        {
            keyWaiting = "keyRun";
            foreach (var bt in _listButtons)
                bt.Enabled = false;
        }

        private void bt_setArea_Click(object sender, EventArgs e)
        {
            keyWaiting = "keyChangeArea";
            foreach (var bt in _listButtons)
                bt.Enabled = false;
        }

        private void bt_setTranslator_Click(object sender, EventArgs e)
        {
            keyWaiting = "keyChangeTranslator";
            foreach (var bt in _listButtons)
                bt.Enabled = false;
        }

        private void bt_setTranslator2_Click(object sender, EventArgs e)
        {
            keyWaiting = "keyChangeTranslator2";
            foreach (var bt in _listButtons)
                bt.Enabled = false;
        }

        private void bt_useTranslator2_Click(object sender, EventArgs e)
        {
            keyWaiting = "keyUseTranslator2";
            foreach (var bt in _listButtons)
                bt.Enabled = false;
        }

        private void bt_fontSizeUp_Click(object sender, EventArgs e)
        {
            keyWaiting = "keyFontSizeUp";
            foreach (var bt in _listButtons)
                bt.Enabled = false;
        }

        private void bt_fontSizeDown_Click(object sender, EventArgs e)
        {
            keyWaiting = "keyFontSizeDown";
            foreach (var bt in _listButtons)
                bt.Enabled = false;
        }
    }
}
